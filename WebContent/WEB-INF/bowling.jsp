<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Bowling Scoring</title>
        <style type="text/css">
            tr.cell td {
                border: 1px solid;
            }
            tr.cell td div {
                min-height: 30px;
                min-width: 30px;
            }
            table#pinButton button{
                font-size: 20px;
            }
        </style>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
       
        <script type="text/javascript">
             var ctx = "${pageContext.request.contextPath}";
            $(document).ready(function() {
               init();
                $('#newGameBtn').click(function(){
                           $.ajax({
                            method: "GET",
                            url: "https://bowlingv1.cfapps.io/scoring",
                            data: {reset: true}
                        }).done(function (msg) {
                                    $('#view').html(msg);
                        });
                    });
                });
            
               function init(){
                    $('#pinButton tr').find('button').each(function () {
                    $(this).bind('click', function () {
                        console.log('clicked');
                        var pinDown = $(this).val();
                        $.ajax({
                            method: "GET",
                            url: "https://bowlingv1.cfapps.io/scoring",
                            data: {pinDown: pinDown}
                        }).done(function (msg) {
                                    $('#view').html(msg);
                                    $('#pinButton').find('button').each(function(){
                                        if($(this).attr('data-style-class') == "disabled"){
                                            $(this).attr('disabled',true);
                                            $(this).css('background-color','#ff0000');
                                        }else{
                                            $(this).attr('disabled',false);
                                            $(this).css('background-color','#FFFFFF');
                                        }
                                    });
                            });
                    });
                });
            }
        </script>
    </head>
    <body>
        <div id="view">
            <div><label>Bowling Scoring</label></div>
            <div style="float:right"><button id="newGameBtn">new Game</button></div>
          
            <table id="pinButton">
                <tbody>
                    <tr>
                        <c:forEach items="${pins}" var ="pin">
                            <td><button value="${pin.value}" data-style-class="${pin.styleClass}">${pin.value}</button></td>
                        </c:forEach>
                    </tr>
                </tbody>
            </table>
            <table>
                <tr>
                    <td>
                        <c:forEach items="${frames}" var ="frame">
                        <td>
                            <table id="frame${frame.idx}">
                                <tbody>
                                    <c:choose>
                                        <c:when test="${frame.idx != 10}">
                                               <tr>
                                        <th colspan="2">${frame.idx}</th>
                                    <tr class="cell">
                                        <td><div>${frame.cells[0].cellContent}</div></td>
                                        <td><div>${frame.cells[2].cellContent}</div></td>
                                    </tr>
                                    <tr class="cell">
                                        <td><div>${frame.cells[1].cellContent}</div></td>
                                        <td><div></div></td>
                                    </tr>
                                        </c:when>
                                        <c:otherwise>
                                               <tr>
                                        <th colspan="2">${frame.idx}</th>
                                    <tr class="cell">
                                        <td><div>${frame.cells[0].cellContent}</div></td>
                                        <td><div>${frame.cells[2].cellContent}</div></td>
                                         <td><div>${frame.cells[3].cellContent}</div></td>
                                    </tr>
                                    <tr class="cell">
                                        <td><div>${frame.cells[1].cellContent}</div></td>
                                        <td><div></div></td>
                                    </tr>
                                        </c:otherwise>
                                    </c:choose>
                                </tbody>
                            </table>
                        </td>
                    </c:forEach>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>