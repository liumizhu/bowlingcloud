/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test.bowling;

/**
 *
 * @author xingliu
 */
public class PinButton {

    private int value;

    private String styleClass = "normal";

    public PinButton(int value) {
        this.value = value;

    }

    public int getValue() {
        return value;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    public String getStyleClass() {
        return styleClass;
    }
}
