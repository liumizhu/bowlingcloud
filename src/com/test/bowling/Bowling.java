package com.test.bowling;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Bowling
 */
@WebServlet("/scoring")
public class Bowling extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private int frameCounter = 1;
    Map<Integer, List<Integer>> scoreMap = new HashMap<>();
    Map<Integer, List<Integer>> strikeMap = new HashMap<>();

    Frame[] frames;
    PinButton[] pinButtons;

    public Bowling() {
        init();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("pinDown") != null) {
            int pinDown = Integer.valueOf(request.getParameter("pinDown"));
            addFurtherThrowToStrikeMap(pinDown);
            checkIfTimeForStrikeScore();
            Frame frame = frames[frameCounter - 1];
            //first throw this frame
            if (!scoreMap.containsKey(frameCounter - 1)) {
                scoreMap.put(frameCounter - 1, new ArrayList<>());
                frame.setCellContentByIdx(0, pinDown, scoreMap);
                //the previous has spare
                if (frameCounter >= 2 && frames[frameCounter - 2].hasSpare()) {
                    Frame previousFrame = frames[frameCounter - 2];
                    previousFrame.getScorCell().setText(String.valueOf(previousFrame.caculateScore(pinDown, frameCounter, frames, scoreMap)));
                }
                disableButtonByLeftedPins(pinDown);
            }
            if (scoreMap.get(frameCounter - 1).size() == 1) {
                frame.setCellContentByIdx(2, pinDown, scoreMap);
            }

            scoreMap.get(frameCounter - 1).add(pinDown);

            //after two throws, time to give the score for this frame
            if (scoreMap.get(frameCounter - 1).size() >= 2) {
                resetPinButton();
                if (!frame.hasStrike() && !frame.hasSpare()) {
                    int scores = frame.caculateScore(pinDown, frameCounter, frames, scoreMap);
                    frame.getScorCell().setText(String.valueOf(scores));
                }
                if (frameCounter != 10) {
                    frameCounter++;
                } else {
                    //the 10th frame
                    if (frame.hasStrike()) {//the very last throw in the last frame
                        disableButtonByLeftedPins(pinDown);
                    }
                    if (scoreMap.get(frameCounter - 1).size() == 3) {
                        frame.setCellContentByIdx(3, pinDown, scoreMap);
                        frame.getScorCell().setText(String.valueOf(frame.caculateScore(pinDown, frameCounter, frames, scoreMap)));
                    }
                }
            }

            if (frame.hasStrike() && frameCounter != 10) {
                strikeMap.put(frameCounter - 1, new ArrayList<>());
                frameCounter++;
            }
        }
        if (request.getParameter("reset") != null) {
            init();
        }

        request.setAttribute("frames", Arrays.asList(frames));
        request.setAttribute("pins", Arrays.asList(pinButtons));
        removeCache(response);
        request.getRequestDispatcher("WEB-INF/bowling.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub

        doGet(request, response);
        removeCache(response);
        request.getRequestDispatcher("/WEB-INF/bowling.jsp").forward(request, response);
    }

    private void addFurtherThrowToStrikeMap(int pinDown) {
        for (Map.Entry entry : strikeMap.entrySet()) {
            ((List<Integer>) entry.getValue()).add(pinDown);
        }
    }

    private void checkIfTimeForStrikeScore() {
        for (Map.Entry entry : strikeMap.entrySet()) {
            if (((List<Integer>) entry.getValue()).size() == 2) {
                int frameIdx = (int) entry.getKey();
                int scoreNextThrows = ((List<Integer>) entry.getValue()).stream().mapToInt(Integer::intValue).sum();
                int previousScore = (frameIdx >= 1 && !frames[frameIdx - 1].getScorCell().getCellContent().isEmpty())
                        ? Integer.valueOf(frames[frameIdx - 1].getScorCell().getCellContent()) : 0;
                frames[frameIdx].getScorCell().setText(String.valueOf(scoreNextThrows + 10 + previousScore));
            }
        }
    }

    private void disableButtonByLeftedPins(int pinDown) {
        int leftedPins = 10 - Integer.valueOf(pinDown);
        for (PinButton pb : pinButtons) {
            if (pb.getValue() > leftedPins && pinDown != 10) {
                pb.setStyleClass("disabled");
            }
        }
    }

    private void resetPinButton() {
        for (PinButton pb : pinButtons) {
            pb.setStyleClass("ready");
        }
    }

    public void init() {
        frames = new Frame[]{
            new Frame(1),
            new Frame(2),
            new Frame(3),
            new Frame(4),
            new Frame(5),
            new Frame(6),
            new Frame(7),
            new Frame(8),
            new Frame(9),
            new Frame(10)};
        pinButtons = new PinButton[]{
            new PinButton(0),
            new PinButton(1),
            new PinButton(2),
            new PinButton(3),
            new PinButton(4),
            new PinButton(5),
            new PinButton(6),
            new PinButton(7),
            new PinButton(8),
            new PinButton(9),
            new PinButton(10)};
        scoreMap = new HashMap<>();
        strikeMap = new HashMap<>();
        frameCounter = 1;
    }

    private void removeCache(HttpServletResponse httpServletResponse){
        httpServletResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        httpServletResponse.setHeader("Pragma", "no-cache");
        httpServletResponse.setDateHeader("Expires", 0);
    }
}
