package com.test.bowling;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class Frame implements Serializable {

    private int idx;
    private Cell[] cells;
    private boolean hasStrike = false;
    private boolean hasSpare = false;

    Frame(int idx) {
        this.idx = idx;
        cells = new Cell[4];
        for (int i = 0; i < cells.length; ++i) {
            cells[i] = new Cell();

        }
    }

    public Cell[] getCells() {
        return cells;
    }

    public int getIdx() {
        return idx;
    }

    Cell getScorCell() {
        return cells[1];
    }

    public void setCellContentByIdx(int cellIdx, int pinDown, Map<Integer, List<Integer>> scoreMap) {
        cells[cellIdx].setText(String.valueOf(pinDown));
        if (cellIdx == 2) {
            int scoresThrow1 = scoreMap.get(idx - 1).stream().mapToInt(Integer::intValue).sum();
            int scores = scoresThrow1 + Integer.valueOf(pinDown);
            if (scores == 10) {
                cells[cellIdx].setText("/");
                hasSpare = true;
                return;
            }
        }
        if (Integer.valueOf(pinDown) == 0) {
            cells[cellIdx].setText("-");
            return;
        }
        if (Integer.valueOf(pinDown) == 10) {
            cells[cellIdx].setText("X");
            hasStrike = true;
            return;
        }
    }

    public boolean hasSpare() {
        return hasSpare;
    }

    public boolean hasStrike() {
        return hasStrike;
    }

    public class Cell {

        private String cellContent;

        void setText(String text) {
            this.cellContent = text;
        }

        public String getCellContent() {
            return cellContent;
        }

    }

    public int caculateScore(int pinDown, int frameCounter, Frame[] frames, Map<Integer, List<Integer>> scoreMap) {
        if (hasSpare) {
            int scorePreviousValue = frameCounter >= 3 ? Integer.valueOf(frames[frameCounter - 3].getScorCell().getCellContent()) : 0;
            int scoreBonus = 10 + pinDown;
            return scoreBonus + scorePreviousValue;
        } else {
            int scores = scoreMap.get(frameCounter - 1).stream().mapToInt(Integer::intValue).sum();
            int scorePreviousValue = frameCounter >= 2 ? Integer.valueOf(frames[frameCounter - 2].getScorCell().getCellContent()) : 0;
            return scores + scorePreviousValue;
        }

    }

}
